> Following text assumes that you have [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) managed as a non-root user and starting on boot (see [Post-installation steps](https://docs.docker.com/install/linux/linux-postinstall/)) and [Swarm mode enabled](https://docs.docker.com/engine/swarm/swarm-mode/).

# How to deploy Docker Swarm Admin Stack

Don't forget to change domains in compose files!!!

## Create overlay proxy network

```
docker network create -d overlay traefik
```

## Create password for Traefik admin UI and store it in Docker secrets

```
echo $(htpasswd -nb admin <password>) | docker secret create traefik-pass -
```

## Create password for Portainer and store it in Docker secrets

```
echo -n <password> | docker secret create portainer-pass -
```

## Deploy admin-stack

```
docker stack deploy -c docker-compose.admin.yml admin-stack
```

# How to deploy Rails app

## Build image

```
docker build --rm -f "rails-app/Dockerfile" -t local/rails-app "rails-app"
```

## Start-up your application

```
docker stack deploy -c docker-compose.yml rails-app
```

# Find Rails app container ID

```
docker ps
```

## Create database

```
docker exec <ContainerID> bundle exec rake db:create db:migrate
```

## Import data

```
docker exec <ContainerID> bundle exec rake db:seed
```

## Index data to Elasticsearch

```
docker exec <ContainerID> bundle exec rake searchkick:reindex:all
```

## Clean up

```
docker image prune -f
docker container prune -f
```
