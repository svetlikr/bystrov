Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get '/o-autorovi', to: 'static_pages#author', as: 'author'
  get '/o-pruvodci', to: 'static_pages#about', as: 'about'
  resources :camps, only: [:index, :show, :search], path: '/tabory', as: 'camps'
  root :to => "camps#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end