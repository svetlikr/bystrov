json.extract! camp, :id, :name, :content, :lat, :lng, :created_at, :updated_at
json.url camp_url(camp, format: :json)
