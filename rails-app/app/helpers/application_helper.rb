module ApplicationHelper
  def markdown(text)
    options = {
      filter_html:     true,
      hard_wrap:       true,
      # link_attributes: { rel: 'nofollow', target: "_blank" },
      link_attributes: { rel: 'nofollow' },
      space_after_headers: true,
      fenced_code_blocks: true
    }

    extensions = {
      autolink:           true,
      superscript:        true,
      footnotes:          true, 
      disable_indented_code_blocks: true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end
  def camps_links(code)
    code = code.gsub('\<strong>(.*?)\<\/strong\>','$1')
  end
end
