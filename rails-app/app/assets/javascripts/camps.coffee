# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $ ->
    $('[data-toggle="tooltip"]').tooltip()
    $('.camp-content strong>a').tooltip()
    return

  if sessionStorage.getItem('map-state') == 'map-open'
    $('#map').removeClass 'map-close'
    $('#map').addClass 'map-open'
    $('#toogler>i').removeClass 'fa-rotate-180'
    $('#toogler>i').addClass 'normal'
  if sessionStorage.getItem('map-state') == 'map-close'
    $('#map').removeClass 'map-open'
    $('#map').addClass 'map-close'
    $('#toogler>i').removeClass 'normal'
    $('#toogler>i').addClass 'fa-rotate-180'

  $('#toogler').click ->
    if sessionStorage.getItem('map-state') == 'map-open' || sessionStorage.getItem('map-state') == null
      sessionStorage.setItem 'map-state', 'map-close'
    else
      sessionStorage.setItem 'map-state', 'map-open'
    $('#map').toggleClass 'map-open map-close'
    setTimeout (->
      $('#toogler>i').toggleClass 'normal fa-rotate-180'
      return
    ), 700
    setTimeout (->
      map.invalidateSize()
      return
    ), 800
    return

  $(window).scroll ->
    if $(this).scrollTop() > 100
      $('#back-to-top').fadeIn()
    else
      $('#back-to-top').fadeOut()
    return

  $('#back-to-top').click ->
    $('#back-to-top').tooltip 'hide'
    $('body,html').animate { scrollTop: 0 }, 800
    false

  $('.camp-content strong>a').attr(
    'data-toogle': 'tooltip'
    'data-placement': 'right'
    'title': 'Vyhledat')

  carto = new (L.tileLayer)('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png',
    maxZoom: 18
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>')

  map = new (L.map)('map',
    maxBounds:[[-180,-180],[180, 360]]
    maxBoundsViscosity: 1.0
    minZoom: 1)
  map.fitBounds([
      [71, 182],
      [38, 18]
    ])
  map.addLayer(carto)

  campId = Number($('.camp-content').attr('data-id'))
  campName = Number($('.camp-content').attr('name'))

  markerOptions = 
    radius: 8
    fillColor: '#ed1c24'
    color: '#000'
    weight: 1
    opacity: 0
    fillOpacity: 1

  $.ajax
    dataType: 'text'
    url: '/tabory.json'
    success: (data) ->
      geojson = $.parseJSON(data)
      onEachFeature = (feature, layer) ->
        layer.bindPopup '<div class="popup"><strong><a href="/tabory/'+feature.properties.id+'">'+feature.properties.name+'</a></strong></div>',
          closeButton: false

      featureLayer = L.geoJSON(geojson,
                              onEachFeature: onEachFeature,
                              pointToLayer: (feature, latlng) ->
                                L.circleMarker latlng, markerOptions)

      map.fitBounds featureLayer.getBounds()

      markers = L.markerClusterGroup(
        polygonOptions: { weight: 1.5, color: '#222', opacity: 0.5 }
        )
      markers.addLayer(featureLayer)
      markers.addTo map

      markers.eachLayer (marker) ->
        content = campName
        if marker.feature.properties.id == campId
          map.setView marker.getLatLng(), 10
          L.popup(
            closeButton: false
            ).setLatLng(marker.getLatLng()).setContent(marker._popup._content).openOn(map)
         return

  map.addControl new (L.Control.Search)(
    url: 'http://nominatim.openstreetmap.org/search?format=json&q={s}'
    jsonpParam: 'json_callback'
    propertyName: 'display_name'
    propertyLoc: [
      'lat'
      'lon'
    ]
    marker: L.circleMarker([
      0
      0
    ], 
      radius: 8
      fillColor: '#OOO'
      weight: 1
      opacity: 0
      fillOpacity: 0.8
      className: 'blinking'

    )
    autoCollapse: true
    autoType: false
    minLength: 2)

  return
  


