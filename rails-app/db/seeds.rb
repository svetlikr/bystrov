# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ruby encoding: utf-8

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
Camp.destroy_all

require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'camps-import.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'utf-8')
csv.each do |row|
  c = Camp.new
  c.name = row['Name']
  c.content = row['Content']
  c.lat = row['Lat']
  c.lng = row['Lng']
  c.save
  puts "#{c.name} saved"
end

puts "There are now #{Camp.count} rows in the camps table"


